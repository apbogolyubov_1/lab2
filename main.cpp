#include <vector>
#include <iostream>
#include <typeinfo>
#include <cmath>

using namespace std;

template <typename Type1>
class Matrix {
Type1 *main_array;
unsigned num_rows;
unsigned num_collums;
public:
    const unsigned getr(){
        return num_rows;
    }
    const unsigned getc(){
        return num_collums;
    }
    Matrix( int c = 1) {
        num_collums = c;
        num_rows = 1;
        main_array = new Type1[num_collums];
        for (int i = 0; i < num_collums; ++i) {
            main_array[i] = 0;
        }
    }

    Matrix(int c = 1, int r = 1) {
        num_rows = r;
        num_collums = c;
        main_array = new Type1[num_rows * num_collums];
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_collums + j] = 0;
            }
        }
    }

    Matrix(const std::initializer_list<Type1> &list) {
        num_collums = list.size();
        num_rows = 1;
        main_array = new Type1[num_collums];
        for (int i = 0; i < num_collums; ++i) {
            main_array[i] = *(list.begin() + i);
        }
    }

    Matrix(const std::initializer_list<std::initializer_list<Type1>> &list) {
        int m = 0;
        for (int i = 0; i < list.size(); ++i){
            m = (m < (list.begin() + i)->size()) ? (list.begin() + i)->size() : m;
        }
        num_collums = m;
        num_rows = list.size();
        main_array = new Type1[num_rows * num_collums];
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < (list.begin() + i)->size(); ++j) {
                main_array[i * num_collums + j] = *((list.begin() + i)->begin() + j);
            }
            for (int j = (list.begin() + i)->size(); j < m; ++j){
                main_array[i * num_collums + j] = 0;
            }
        }
    }

    Matrix(const Matrix<Type1> &other){
        num_collums = other.num_collums;
        num_rows = other.num_rows;
        main_array = new Type1[num_rows * num_collums];
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_collums + j] = other.main_array[i * num_collums + j];
            }
        }
    }

    Matrix<Type1> &operator=(const Matrix<Type1> &other) {
        delete[] main_array;
        num_rows = other.num_rows;
        num_collums = other.num_collums;
        main_array = new Type1[num_rows * num_collums];
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_collums + j] = other.main_array[i * other.num_collums + j];
            }
        }
        return *this;
    }

    Matrix(const Matrix<Type1> &&other){
        num_collums = other.num_collums;
        num_rows = other.num_rows;
        main_array = other.main_array;
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_collums + j] = other.main_array[i * num_collums + j];
            }
        }
    }

    Type1 &operator()(int i, int j){
        return main_array[i * num_collums + j];
    }

    friend std::ostream &operator<<(std::ostream &out, const Matrix<Type1> &mat) {
        for (int i = 0; i < mat.num_rows; ++i) {
            for (int j = 0; j < mat.num_collums; ++j) {
                out << mat.main_array[i * mat.num_collums + j] << "\t";
            }
            out << "\n";
        }
        return out;
    }

    Matrix<Type1> operator+=(const Matrix<Type1> &other) {
        if (num_rows != other.num_rows || num_collums != other.num_collums) {
            std::cout << "Unequal matrixes"<< "\n";
            return *this;
        }
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_rows + j] += other.main_array[i * other.num_rows + j];
            }
        }
        return *this;
    }

    friend Matrix<Type1> operator+(Matrix<Type1> fst, const Matrix<Type1> &scnd) {
        fst += scnd;
        return fst;
    }

    Matrix<Type1> operator-=(const Matrix<Type1> &other) {
        if (num_rows != other.num_rows || num_collums != other.num_collums) {
            std::cout << "Unequal matrixes"<< "\n";
            return *this;
        }
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_rows + j] -= other.main_array[i * other.num_rows + j];
            }
        }
        return *this;
    }

    friend Matrix<Type1> operator-(Matrix<Type1> fst, const Matrix<Type1> &scnd) {
        fst -= scnd;
        return fst;
    }

    Matrix<Type1> &operator*=(Type1 koef) {
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < num_collums; ++j) {
                main_array[i * num_collums + j] *= koef;
            }
        }
        return *this;
    }

    friend Matrix<Type1> operator*(Type1 koef, Matrix<Type1> mat) {
        mat *= koef;
        return mat;
    }

    friend Matrix<Type1> operator*(Matrix<Type1> &fst, Matrix<Type1> &scnd) {
        if (fst.num_collums != scnd.num_rows) {
            std::cout << "Unmatching matrix sizes"<< "\n";
            return fst;
        }
        Matrix<Type1> tmp(fst.num_rows, scnd.num_collums);
        for (int i = 0; i < fst.num_rows; ++i) {
            for (int j = 0; j < scnd.num_collums; ++j) {
                for (int k = 0; k < fst.num_collums; ++k) {
                    tmp(i, j) += fst(i,k) * scnd(k,j);
                }
            }
        }
        return tmp;
    }

    template<typename Type2>
    Matrix(const std::initializer_list<std::initializer_list<Type2>> &list){
        int m = 0;
        for (int i = 0; i < list.size(); ++i){
            m = (m < (list.begin() + i)->size()) ? (list.begin() + i)->size() : m;
        }
        num_collums = m;
        num_rows = list.size();
        main_array = new Type1[num_rows * num_collums];
        for (int i = 0; i < num_rows; ++i) {
            for (int j = 0; j < (list.begin() + i)->size(); ++j) {
                main_array[i * num_collums + j] = *((list.begin() + i)->begin() + j);
            }
            for (int j = (list.begin() + i)->size(); j < m; ++j){
                    main_array[i * num_collums + j] = 0;
            }
        }

    }//Для разных типов почему то получилось написать только это,
    // операторы не работали

    template<typename Type2>
    Matrix(const std::initializer_list<Type2> &list){
        num_collums = list.size();
        num_rows = 1;
        main_array = new Type1[num_collums];
        for (int i = 0; i < num_collums; ++i) {
            main_array[i] = floor(double(*(list.begin() + i)));
        }

    }

    //template<typename Type2>
    //Matrix<Type1> &operator=(const Matrix<Type2> &other) {
    //    delete[] main_array;
    //    num_rows = other.getr();
    //    num_collums = other.n;
    //    main_array = new Type1[num_rows * num_collums];
    //    for (int i = 0; i < num_rows; ++i) {
    //        for (int j = 0; j < num_collums; ++j) {
    //            main_array[i * num_collums + j] = other.main_array[i * other.num_collums + j];
    //        }
    //    }
    //    return *this;
    //}

    ~Matrix() {
        delete[] main_array;
    }
};

int main ()
{
    Matrix<int> m1 = { {1, 2, 3}, {4, 5, 6} };
    Matrix<int> m2 = { {7, 8, 9}, {2, 3} };
    std::cout << m1 << std::endl;
    std::cout << m2 << std::endl;
    m1 += m2;
    std::cout << m1 << std::endl;
    Matrix<int> m3 = m1 * m2;
    Matrix<int> m4 = { {1, 2, 3}, {4, 5, 6},{7, 8, 9}};
    Matrix<int> m5 = { {1, 0, 0}, {0, 1, 0},{0, 0, 1}};
    Matrix<int> m6 = m4 * m5;
    std::cout << m6 << std::endl;
    Matrix<double> m7 = { {1, 0, 0}, {0, 1, 0},{0, 0, 1}};
    std::cout << m7 << std::endl;
    std::initializer_list<std::initializer_list<double>> list = {{1.2, 3.4, 0.5}, {0.8, 1.7, 0.4},{0.6, 0.2, 1.9}};
    Matrix<int> m8 = list;
    std::cout << m8 << std::endl;
    Matrix<double> m9 = { 1,2,3,4,5,6};
    std::cout << m9 << std::endl;
    std::initializer_list<double> list2 = {1.2, 3.4, 0.5};
    Matrix<int> m10 = list2;
    std::cout << m10 << std::endl;
}
